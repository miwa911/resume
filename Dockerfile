FROM nginx:1.12-alpine
MAINTAINER  hung.dang980@gmail.com

COPY index.html /usr/share/nginx/html
COPY images /usr/share/nginx/html