var theme = require('./index')
var fs = require('fs');
var resume = require('./resume.json');
var pdf = require('html-pdf');
var html = (theme.render(resume));

fs.writeFile("./index.html",html, function(err) {
  if(err) {
      return console.log(err);
  }

  console.log("The file was saved!");
});

var options = { format: 'Letter' };
pdf.create(html, options).toFile('./resume.pdf', function(err, res) {
  if (err) return console.log(err);
  console.log(res); // { filename: '/app/businesscard.pdf' }
});
